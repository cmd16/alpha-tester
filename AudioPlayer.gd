extends AudioStreamPlayer

var sounds = {}

func add_sound(sound_name, path_name):
	var new_stream = load(path_name)
	self.sounds[sound_name] = new_stream

func play_sound(sound_name):
	print("playing sound: ", sound_name)
	if !self.sounds.has(sound_name):
		print("skipping sound: ", sound_name)
		return
	self.stream = self.sounds[sound_name]
	self.play()

func play_sounds(sound_list):
	for sound in sound_list:
		self.play_sound(sound)
		yield(self, "finished")
