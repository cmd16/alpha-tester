extends Node2D

var Game = preload("res://Game.tscn")
var puzzle_save_file = "user://puzzle.dat"
var settings_save_file = "user://settings.dat"

const puzzle_list = ["parse", "locate_font", "title", "translation", "x",
"overflow", "sprite", "collision", "library", "money", "solved"]

const CHANNELS = ["music", "voice", "sfx"]

var puzzle_info = {
	"parse": {
		"error": "Parsing error: unexpected end of file. Sumbit line number of missing close parentheses.",
		"answer": "220",
		"comment": "Oh, come on! I could have sworn I fixed that. But hey, that's the kind of stuff you're here to find.",
		"hints": [
			"I'm pretty sure I've got the game code somewhere on my computer.",
			"I think Game.gd is the main script file. Try looking there.",
			"There's somewhere in the file with an open parentheses and no close parentheses. I think it was near the end of the file."
		]
	},
	"locate_font": {
		"error": "Missing font file. Submit file path.",
		"answer": "assets/fonts/joystix.ttf",
		"comment": "Lesson learned: don't decide last minute to reorganize everything.",
		"hints": [
			"The answer is in the codebase somewhere, I'm sure.",
			"I usually put stuff like fonts in the assets folder.",
			"Have you looked at assets/fonts yet?"
		]
	},
	"title": {
		"error": "Missing game title. Submit title.",
		"answer": "Quackout",
		"comment": "Did I seriously leave the placeholder text in? After we came up with such a cool title?",
		"hints": [
			"I think this one's missing from the codebase. Maybe there's someone you could talk to?",
			"Now I remember! I didn't come up with the title, my friend did.",
			"If you tell her you need the game title, I'm sure she can provide it."
		]
	},
	"translation": {
		"error": "Roqungxi uglh okqbef. Zjmqil xns nsr ytqflgychyvfcsuaoowfcoxgfbupkxzoc.",
		"key": "godotgreatdontcrashplease",
		"error_translated": "Language unit broken. Submit the key scmfsdkpowefklfpkoebwausmvdlkewap",
		"answer": "scmfsdkpowefklfpkoebwausmvdlkewap",
		"comment": "Really? I had that in as a joke. I thought I took it out though. Guess not.",
		"hints": [
			"This is a cool cipher I added in for no good reason. I was just talking about ciphers with a friend not long ago.",
			"As for the key, I had some ideas in my notes. Of course, I'd never put the whole key in one file.",
			"I think the key ended up being a combination of an epic name and a cool name.",
			"If you look at the names, you'll see most of them have one thing in common. The pieces of the key are the odd ones out.",
			"Most of the names are puns, but not all. They key is non-pun epic name + non-pun cool name.",
			"Once you have the key, you'll need to use it to decode the cipher. You may find this website useful: https://www.boxentriq.com/code-breaking/vigenere-cipher"
		]
	},
	"x": {
		"error": "Cannot have a variable named 'x'. Submit correct variable name.",
		"answer": "boingboikillcount",
		"comment": "Seriously? The linter (that's a checker for code style) just now decides to kick in?",
		"hints": [
			"Hmmm, this was another combination of names I think.",
			"If you look at the codebase, you can see what variable is named 'x' and how it is used.",
			"I think this one was a fun name and an epic name.",
			"As with the previous problem, this requires you to find the odd one out.",
			"Most of the names are puns, but not all. They key is non-pun fun name + non-pun epic name."
		]
	},
	"overflow": {
		"error": "Cannot have -9999 bricks. Suspected overflow error. Submit correct number of bricks.",
		"answer": "84",
		"comment": "It's a shame I can't have a type that only allows positive integers. At least the sanity check ran.",
		"hints": [
			"Look for the code where the bricks are created.",
			"I recommend investigating the function set_bricks.",
			"If you know how many rows there are and how many columns there are, that should tell you how many bricks there are."
		]
	},
	"sprite": {
		"error": "Multiple images are available. Submit the name of the player sprite.",
		"answer": "rubberduck.png",
		"comment": "I thought I changed that name to 'player' to avoid this very problem! Seems that didn't go through.",
		"hints": [
			"Take a look at the images. Maybe something there looks like it could be a player sprite?",
			"I would not use a book as a sprite.",
			"Once you've found a promising image, you'll need to figure out the name.",
			"I don't know about you, but that image looks like a rubber duck to me."
		]
	},
	"collision": {
		"error": "Missing collision shape for player. Submit name of collision shape.",
		"answer": "octodecahedron",
		"comment": "Since when do collision shapes have names? Isn't it just about adding points?",
		"hints": [
			"You'll need to find the Player code that has a CollisionPolygon2D.",
			"You should be able to see what points are in the shape. If you count them up, that will give you the name of the polygon.",
			"If you don't know how to name that kind of polygon, check out my note on geometry.",
			"Unfortunately, I think I had a typo. I always get the suffixes confused for 2d shapes vs 3d shapes."
		]
	},
	"library": {
		"error": "Missing SFX library. Submit decimal code for SFX library.",
		"answer": "600.700",
		"comment": "That's what I get for using external code. More compatibility issues.",
		"hints": [
			"Have you found the bookshelf images yet? That's probably relevant.",
			"So I think this library thing involved two sets of books: one set was 3 identical books, and I think they were by Sam? The other had some books on display stands.",
			"The library name is related to the filenames you found the books in. But the format is a bit different.",
			"Have you heard of the Dewey Decimal System? Try 2 Dewey Decimal numbers together.",
			"No need to get specific: the 100-level categories are fine.",
			"It's a decimal system, so try putting a decimal point somewhere."
		]
	},
	"money": {
		"error": "Submit financial account username to setup microtransactions.",
		"answer": "n0tar34lbankacc0unt",
		"comment": "Nooooo! I turned those off! I made it in-game currency only. This shouldn't be happening.",
		"hints": [
			"Customer Service should help get my username.",
			"Right, the security questions! First pet: I think I made a reference to a video game. Some kind of bird, but not a duck. Maybe there's a clue in my notes?",
			"Favorite teacher: another reference. This one is an animal too, and also a programming language. Again, notes may help you out.",
			"Home street: I considered myself so clever for this one. It's not even on earth!"
		]
	}
}
#var current_puzzle = puzzle_list[0]
var current_puzzle = "solved"
var restart_count = 0
var hint_number = 0
var comment_given = false

func _ready():
	Console.add_command('submit', self, 'submit_answer')\
		.set_description('Submits "%text%" as the answer to the current problem.')\
		.add_argument('text', TYPE_STRING)\
		.register()
	Console.add_command('run', self, 'launch_game')\
		.set_description('Launches the game')\
		.register()
	Console.add_command('hint', self, 'give_hint')\
		.set_description('Gives a hint')\
		.register()
	Console.add_command('repeat', self, 'repeat_something')\
		.set_description('Repeats latest comment or hint, whichever is specified in "%text%".')\
		.add_argument('text', TYPE_STRING)\
		.register()
	Console.add_command('reset', self, 'reset_something')\
		.set_description('Resets hint, errors (resets puzzles), or game (the game within this game), whichever is specified in "%text%".')\
		.add_argument('text', TYPE_STRING)\
		.register()
	Console.add_command('mute', self, 'mute')\
		.set_description('Mute the audio specified in "%channel%" (music, voice, or sfx). If no channel specified, mute everything.')\
		.add_argument('channel', TYPE_STRING)\
		.register()
	Console.add_command('unmute', self, 'unmute')\
		.set_description('Unmute the audio specified in "%channel%" (music, voice, or sfx). If no channel specified, unmute everything.')\
		.add_argument('channel', TYPE_STRING)\
		.register()
	Console.add_command('volume', self, 'volume')\
		.set_description('Set the volume for "%channel%" (music, voice, or sfx) to the level specified in %level% (1 is quietest, 10 is loudest).')\
		.add_argument('channel', TYPE_STRING)\
		.add_argument('channel', TYPE_INT)\
		.register()
	Console.add_command('skip_puzzles', self, 'skip_puzzles')\
		.set_description('Skip all puzzles, so you can get to the non-puzzle part of the game. For developers (or cheaters).')\
		.register()
	var file = File.new()
	if file.file_exists(self.puzzle_save_file):
		file.open(self.puzzle_save_file, File.READ)
		self.current_puzzle = file.get_var()
	file.close()
	if self.puzzle_list.find(self.current_puzzle) == 0:
		Console.write_line("""Welcome! Thanks again for agreeing to test my game. You can get around in here via various terminal commands. The main ones are [url]run[/url] to run the game and [url]submit[/url] to submit a fix for any bugs you find, but you can use [url]commands[/url] to get the full list.""")
	else:
		Console.write_line("Welcome back!")
	Console.toggle_console()

func skip_puzzles():
	self.to_puzzle(self.puzzle_list.size() - 1)

func mute(channel):
	if channel:
		var idx = CHANNELS.find(channel)
		if idx == -1:
			Console.Log.error("Channel not found. Must be music, voice, or sfx.")
			return
		AudioServer.set_bus_mute(idx, true)
	else:
		for i in 3:
			AudioServer.set_bus_mute(i, true)

func unmute(channel):
	if channel:
		var idx = CHANNELS.find(channel)
		if idx == -1:
			Console.Log.error("Channel not found. Must be music, voice, or sfx.")
			return
		AudioServer.set_bus_mute(idx, false)
	else:
		for i in 3:
			AudioServer.set_bus_mute(i, false)

func volume(channel, level):
	var idx = CHANNELS.find(channel)
	if idx == -1:
		Console.Log.error("Channel not found. Must be music, voice, or sfx.")
		return
	var volumes = [-54.0, -48.0, -42.0, -36.0, -30.0, -24.0, -18.0, -12.0, -6.0, 0.0]
	AudioServer.set_bus_volume_db(idx, volumes[clamp(level - 1, 0, volumes.size())])

func save_settings():
	var file = File.new()
	file.open(self.settings_save_file, File.WRITE)
	for i in range(1, 4):
		file.store_float(AudioServer.get_bus_volume_db(i))
	file.close()

func read_settings():
	var file = File.new()
	if !file.file_exists(self.settings_save_file):
		return
	file.open(self.settings_save_file, File.WRITE)
	for i in range(1, 4):
		AudioServer.set_bus_volume_db(i, file.get_float())
	file.close()

func submit_answer(text=''):
	if self.current_puzzle == "solved":
		Console.write_line("No more puzzles to solve.")
	if text == self.puzzle_info[self.current_puzzle]["answer"]:
		Console.write_line("Error resolved!")
		var index = self.puzzle_list.find(self.current_puzzle)
		if index < self.puzzle_list.size() - 1:
			self.next_puzzle()
	else:
		incorrect_answer(text)

func next_puzzle():
	var index = self.puzzle_list.find(self.current_puzzle)
	self.to_puzzle(clamp(index + 1, 0, self.puzzle_list.size() - 1))

func to_puzzle(idx):
	self.current_puzzle = self.puzzle_list[idx]
	self.restart_count = 0
	self.hint_number = 0
	self.comment_given = false
	var file = File.new()
	file.open(self.puzzle_save_file, File.WRITE)
	file.store_var(self.current_puzzle)
	file.close()

func incorrect_answer(text=''):
	Console.Log.error("Incorrect information.")
	if self.current_puzzle == "translation" and text == "godotgreatdontcrashplease":
		Console.write_line("Correct key. Decode error message using this key and correct cipher type.")

func give_hint():
	if self.hint_number >= self.puzzle_info[self.current_puzzle]["hints"].size():
		Console.write_line("No more hints. Type [url]repeat hint[/url] to repeat latest hint or [url]reset hint[/url] to go back to the first hint.")
	else:
		Console.write_line(self.puzzle_info[self.current_puzzle]["hints"][self.hint_number])
		self.hint_number += 1

func reset_something(text):
	if text == "hint":
		self.reset_hint()
	elif text == "errors":
		self.to_puzzle(0)
	elif text == "game":
		var user_files = ["user://score.save", "user://purchases.dat",
		"user://colors.dat", "user://rotation.dat", "user://fast.dat"]
		var dir = Directory.new()
		for file in user_files:
			dir.remove(file)

func repeat_something(text):
	if text == "hint":
		self.repeat_hint()
	elif text == "comment":
		self.print_comment()

func repeat_hint():
	Console.write_line(self.puzzle_info[self.current_puzzle]["hints"][(self.hint_number - 1) if self.hint_number > 0 else 0])

func reset_hint():
	self.hint_number = 0
	self.give_hint()

func print_comment():
	Console.write_line(self.puzzle_info[self.current_puzzle]["comment"])

func launch_game(log_error=true):
	if self.current_puzzle == "parse":
		Console.Log.error(self.puzzle_info[self.current_puzzle]["error"])
		return
	var game = Game.instance()
	game.connect("restart_game", self, "restart_game")
	game.connect("game_over", self, "show_console")
	self.add_child(game)
	if Console.is_console_shown:
		Console.toggle_console()
	if self.current_puzzle == "locate_font":
		get_tree().call_group("text", "hide")
	if self.puzzle_list.find(self.current_puzzle) <= self.puzzle_list.find("sprite"):
		game.get_node("Player").call_deferred("queue_free")
	if self.puzzle_list.find(self.current_puzzle) <= self.puzzle_list.find("translation"):
		game.scramble_text()
	if self.puzzle_list.find(self.current_puzzle) <= self.puzzle_list.find("overflow"):
		get_tree().call_group("brick", "hide")
	if self.puzzle_list.find(self.current_puzzle) <= self.puzzle_list.find("library"):
		AudioServer.set_bus_mute(3, true)
	if self.current_puzzle == "title":
		game.get_node("Title").text = "?????????"
	elif self.current_puzzle == "x":
		game.get_node("ScoreLabel").text = "Score: ???"
	elif self.current_puzzle == "collision":
		game.get_node("Player").collision_layer = 0
		game.get_node("Player").collision_mask = 0
	if self.current_puzzle != "solved":
		game.get_node("Purchases").connect("pressed", game, "quit_game")
		$Timer.connect("timeout", game, "quit_game")
		$Timer.start()
		if log_error:
			Console.Log.error(self.puzzle_info[self.current_puzzle]["error"])
			if !self.comment_given:
				self.print_comment()

func restart_game():
	$Timer.stop()
	if self.current_puzzle != "solved":
		restart_count += 1
	self.remove_child(self.get_node("Game"))
	if restart_count < 5:
		self.launch_game(false)
	else:
		show_console()

func show_console():
	if !Console.is_console_shown:
		Console.toggle_console()
