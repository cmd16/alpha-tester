extends Node2D

var puzzle_list = ["parse", "locate_font", "title", "translation", "command", "x",
"overflow", "sprite", "collision", "date", "library", "money"]

var puzzle_info = {
	"locate_font": {
		"error": "Missing font file. Submit file path.",
		"answer": "assets/fonts/BestFontEver.ttf"
	},
	"sprite": {
		"error": "Multiple sprites are selected. Submit the name of the player sprite.",
		"answer": "RubberDuck.png"
	},
	"money": {
		"error": "Submit financial account username to setup microtransactions.",
		"answer": "n0tar34lbankacc0unt"
	},
	"overflow": {
		"error": "Cannot have -9999 blocks. Suspected overflow error. Submit correct number of blocks.",
		"answer": "56"
	},
	"title": {
		"error": "Missing game title. Submit title.",
		"answer": "Quackout"
	},
	"collision": {
		"error": "Missing collision shape for player. Submit name of collision shape.",
		"answer": "hendecahedron"
	},
	"x": {
		"error": "Cannot have a variable named 'x'. Submit correct variable name.",
		"answer": "boingboikillcount"
	},
	"date": {
		"error": "Datetime module not working. Submit today's date.",
		"answer": "04/04:19:42"
	},
	"parse": {
		"error": "Parsing error: unexpected end of file. Sumbit line number of missing close parentheses.",
		"answer": "1362"
	},
	"command": {
		"error": "Missing initialization command. Submit name of setup function",
		"answer": "the_actual_final_init_42"
	},
	"library": {
		"error": "Missing SFX library. Submit decimal code for SFX library.",
		"answer": "600.700"
	},
	"translation": {
		"error": "Roqungxi uglh okqbef. Zjmqil xns nsr ytqflgychyvfcsuaoowfcoxgfbupkxzoc.",
		"key": "godotgreatdontcrashplease",
		"error_translated": "Language unit broken. Submit the key scmfsdkpowefklfpkoebwausmvdlkewap",
		"answer": "scmfsdkpowefklfpkoebwausmvdlkewap"
	}
}
var current_puzzle = puzzle_list[0]

# Function that will be called by our command
func submit_answer(text = ''):
	if text == self.puzzle_info[self.current_puzzle]["answer"]:
		Console.write_line("Error resolved!")
		var index = self.puzzle_list.find(self.current_puzzle)
		if index < self.puzzle_list.size() - 1:
			self.current_puzzle = self.puzzle_list[index + 1]
	else:
		incorrect_answer()

func incorrect_answer():
	Console.Log.error("Incorrect information.")

func _ready():
	# Registering command
	# 1. argument is command name
	# 2. arg. is target (target could be a funcref)
	# 3. arg. is target name (name is not required if it is the same as first arg or target is a funcref)
	Console.add_command('submit', self, 'submit_answer')\
		.set_description('Submits "%text%" as the answer to the current problem')\
		.add_argument('text', TYPE_STRING)\
		.register()
	Console.toggle_console()
	Console.Log.error(self.puzzle_info[self.current_puzzle]["error"])
