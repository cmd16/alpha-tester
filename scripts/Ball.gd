extends KinematicBody2D

var direction = Vector2.ZERO

signal ball_hit(collider)

func _ready():
	direction = Vector2(300, 300)

func _physics_process(delta):
	var collision = self.move_and_collide(direction*delta)
	if collision:
		var reflect = collision.remainder.bounce(collision.normal)
		direction = direction.bounce(collision.normal)
		self.move_and_collide(reflect)
		emit_signal("ball_hit", collision.collider)
