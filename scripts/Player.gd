extends KinematicBody2D

var speed = 1000

func _physics_process(delta):
	var direction = Vector2.ZERO
	if Input.is_action_pressed("ui_left"):
		direction.x = speed * -1
	elif Input.is_action_pressed("ui_right"):
		direction.x = speed
	if Input.is_action_pressed("ui_up"):
		direction.y = speed * -1
	elif Input.is_action_pressed("ui_down"):
		direction.y = speed
	self.move_and_collide(direction*delta)
