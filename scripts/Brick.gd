extends StaticBody2D

func hit():
	$Sprite.animation = "explode_out"

func _on_Timer_timeout():
	self.queue_free()


func _on_Sprite_animation_finished():
	if $Sprite.animation == "explode_out":
		self.queue_free()
