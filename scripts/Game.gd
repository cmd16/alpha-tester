extends Node2D

signal restart_game
signal game_over

const Brick = preload("res://Brick.tscn")

var boingboikillcount = 0
var cash = 0

var cash_file = "user://score.save"
var purchases_file = "user://purchases.dat"
var colors_file = "user://colors.dat"
var rotation_file = "user://rotation.dat"
var speed_file = "user://fast.dat"

const Player = preload("res://Player.tscn")
const LWallSFX = preload("res://assets/sfx/LWall.wav")
const RWallSFX = preload("res://assets/sfx/RWall.wav")
const CeilingSFX = preload("res://assets/sfx/Ceiling.wav")
const PlayerSFX = preload("res://assets/sfx/player_lower.wav")
const ImpactSFX = preload("res://assets/sfx/Impact.wav")

const LETTERS = ["a", 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
var rng = RandomNumberGenerator.new()

var colors = [Color.orangered, Color.orange, Color.yellow, Color.mediumspringgreen, 
	Color.dodgerblue, Color.mediumpurple, Color.pink]
onready var hat_dict = {
	"cap": {
		"scene": preload("res://PlayerCap.tscn"),
		"purchased": false,
		"cost": 1000,
		"button": $PurchasesPopup/CapButton,
		"label_button": $PurchasesPopup/CapLabelButton
	},
	"magic": {
		"scene": preload("res://PlayerMagic.tscn"),
		"purchased": false,
		"cost": 1500,
		"button": $PurchasesPopup/MagicButton,
		"label_button": $PurchasesPopup/MagicLabelButton
	},
	"cowboy": {
		"scene": preload("res://PlayerCowboy.tscn"),
		"purchased": false,
		"cost": 3000,
		"button": $PurchasesPopup/CowboyButton,
		"label_button": $PurchasesPopup/CowboyLabelButton
	},
	"bonnet": {
		"scene": preload("res://PlayerBonnet.tscn"),
		"purchased": false,
		"cost": 5000,
		"button": $PurchasesPopup/BonnetButton,
		"label_button": $PurchasesPopup/BonnetLabelButton
	},
	"chef": {
		"scene": preload("res://PlayerChef.tscn"),
		"purchased": false,
		"cost": 10000,
		"button": $PurchasesPopup/ChefButton,
		"label_button": $PurchasesPopup/ChefLabelButton
	}
}
onready var other_purchases = {
	"color": {
		"wall": {
			"cost": 400,
			"purchased": false,
			"button": $PurchasesPopup/WallColorPickerButton,
			"label_button": $PurchasesPopup/WallColorPurchaseButton,
			"color": Color(1, 1, 1, 1)
		},
		"ball": {
			"cost": 600,
			"purchased": false,
			"button": $PurchasesPopup/BallColorPickerButton,
			"label_button": $PurchasesPopup/BallColorPurchaseButton,
			"color": Color(1, 1, 1, 1)
		},
		"hat": {
			"cost": 800,
			"purchased": false,
			"button": $PurchasesPopup/HatColorPickerButton,
			"label_button": $PurchasesPopup/HatColorPurchaseButton,
			"color": Color(1, 1, 1, 1)
		},
		"rows": {
			"cost": 1000,
			"purchased": false,
			"button": $PurchasesPopup/RowColorHBoxContainer,
			"label_button": $PurchasesPopup/RowColorPurchaseButton,
		},
		"player": {
			"cost": 1200,
			"purchased": false,
			"button": $PurchasesPopup/PlayerColorPickerButton,
			"label_button": $PurchasesPopup/PlayerColorPurchaseButton,
			"color": Color(1, 1, 1, 1)
		}
	},
	"rotation": {
		"brick": {
			"cost": 300,
			"purchased": false,
			"button": $PurchasesPopup/BrickRotationSlider,
			"label_button": $PurchasesPopup/BrickRotationPurchaseButton,
			"rotation": 0
		},
		"player": {
			"cost": 900,
			"purchased": false,
			"button": $PurchasesPopup/PlayerRotationSlider,
			"label_button": $PurchasesPopup/PlayerRotationPurchaseButton,
			"rotation": 0
		}
	},
	"speed": {
		"ball": {
			"cost": 10000,
			"purchased": false,
			"button": $PurchasesPopup/BallSpeedSlider,
			"label_button": $PurchasesPopup/BallSpeedPurchaseButton,
			"speed": 300
		},
		"player": {
			"cost": 20000,
			"purchased": false,
			"button": $PurchasesPopup/PlayerSpeedSlider,
			"label_button": $PurchasesPopup/PlayerSpeedPurchaseButton,
			"speed": 1000
		}
	}
}

var current_hat = ""

func _ready():
	self.read_colors()
	self.rng.randomize()
	self.boingboikillcount = 0
	self.set_bricks()
	var file = File.new()
	if file.file_exists(self.cash_file):
		file.open(self.cash_file, File.READ)
		var cash = file.get_64()
		self.update_cash(cash, false)
	else:
		self.cash = 0
	read_purchases()
	read_rotations()
	read_speed()
	for node in get_tree().get_nodes_in_group("hat"):
		node.modulate = self.other_purchases["color"]["hat"]["color"]
	self.get_node("Player").get_node("Sprite").modulate = self.other_purchases["color"]["player"]["color"]
	for key in self.hat_dict:
		var hat = self.hat_dict[key]
		hat["label_button"].connect("pressed", self, "purchase_hat_%s" % key)
		hat["button"].connect("pressed", self, "set_hat_%s" % key)
	file.close()

func read_purchases():
	var file = File.new()
	if file.file_exists(self.purchases_file):
		file.open(self.purchases_file, File.READ)
		for key in self.hat_dict:
			self.hat_dict[key]["purchased"] = file.get_var()
		self.current_hat = file.get_var()
		for pt in self.other_purchases:
			for item in self.other_purchases[pt]:
				self.other_purchases[pt][item]["purchased"] = file.get_var()
	file.close()
	var new_player = self.hat_dict[self.current_hat]["scene"].instance() if self.current_hat else Player.instance()
	new_player.global_position = Vector2(676, 722)
	new_player.set_name("Player")
	self.add_child(new_player)
	set_purchase_buttons()

func set_purchase_buttons():
	for key in self.hat_dict:
		if self.hat_dict[key]["purchased"]:
			self.hat_dict[key]["button"].disabled = false
			self.hat_dict[key]["button"].modulate.a = 1.0
			self.hat_dict[key]["label_button"].disabled = true
		else:
			self.hat_dict[key]["button"].disabled = true
			self.hat_dict[key]["button"].modulate.a = 0.5
			if self.hat_dict[key]["cost"] <= self.cash:
				self.hat_dict[key]["label_button"].disabled = false
			else:
				self.hat_dict[key]["label_button"].disabled = true
	for pt in self.other_purchases:
		for item in self.other_purchases[pt]:
			if self.other_purchases[pt][item]["purchased"]:
				if item == "rows":
					for button in self.other_purchases[pt][item]["button"].get_children():
						button.disabled = false
						button.modulate.a = 1.0
				elif pt == "rotation" or pt == "speed":
					self.other_purchases[pt][item]["button"].editable = true
#					self.other_purchases[pt][item]["button"].modulate.a = 1.0
				else:
					self.other_purchases[pt][item]["button"].disabled = false
					self.other_purchases[pt][item]["button"].modulate.a = 1.0
				self.other_purchases[pt][item]["label_button"].disabled = true
			else:
				if item == "rows":
					for button in self.other_purchases[pt][item]["button"].get_children():
						button.disabled = true
						button.modulate.a = 0.5
				elif pt == "rotation" or pt == "speed":
					self.other_purchases[pt][item]["button"].editable = false
#					self.other_purchases[pt][item]["button"].modulate.a = 0.5
				else:
					self.other_purchases[pt][item]["button"].disabled = true
					self.other_purchases[pt][item]["button"].modulate.a = 0.5
				if self.other_purchases[pt][item]["cost"] <= self.cash:
					self.other_purchases[pt][item]["label_button"].disabled = false
				else:
					self.other_purchases[pt][item]["label_button"].disabled = true

func purchase_hat(hat_name):
	$PurchaseSFX.play()
	self.hat_dict[hat_name]["purchased"] = true
	self.update_cash(self.hat_dict[hat_name]["cost"] * -1)
	self.set_purchase_buttons()
	self.save_purchases()

func purchase_hat_cap():
	self.purchase_hat("cap")

func purchase_hat_magic():
	self.purchase_hat("magic")

func purchase_hat_cowboy():
	self.purchase_hat("cowboy")

func purchase_hat_bonnet():
	self.purchase_hat("bonnet")

func purchase_hat_chef():
	self.purchase_hat("chef")

func set_hat(hat_name):
	self.current_hat = hat_name
	self.save_purchases()
	yield(get_tree().create_timer(0.5), "timeout")
	emit_signal("restart_game")
#	get_tree().reload_current_scene()

func set_hat_cap():
	self.set_hat("cap")

func set_hat_magic():
	self.set_hat("magic")

func set_hat_cowboy():
	self.set_hat("cowboy")

func set_hat_bonnet():
	self.set_hat("bonnet")

func set_hat_chef():
	self.set_hat("chef")

func save_purchases():
	var file = File.new()
	file.open(purchases_file, File.WRITE)
	for key in self.hat_dict:
		file.store_var(self.hat_dict[key]["purchased"])
	file.store_var(self.current_hat)
	for pt in self.other_purchases:
		for item in self.other_purchases[pt]:
			file.store_var(self.other_purchases[pt][item]["purchased"])
	file.close()

func set_bricks():
	for row in range(7):
		for col in range(12):
			var brick = Brick.instance()
			brick.position = Vector2(112 + 125*col, 70 + 55*row)
			brick.modulate = colors[row]
			add_child(brick)

func _on_Ball_ball_hit(collider):
	if collider.name == "Floor":
		emit_signal("restart_game")
#		get_tree().reload_current_scene()
	elif "Brick" in collider.name:
		$ImpactSFX.play()
		self.boingboikillcount += 100
		$ScoreLabel.text = "Score: %d" % self.boingboikillcount
		self.update_cash(100)
		collider.hit()
		yield(get_tree().create_timer(0.25), "timeout")
		if get_tree().get_nodes_in_group("brick").size() == 0:
			emit_signal("restart_game")
	elif "Player" in collider.name:
		$PlayerSFX.play()
	elif "Roof" in collider.name:
		$CeilingSFX.play()
	elif "LWall" in collider.get_parent().name:
		$LWallSFX.play()
	elif "RWall" in collider.get_parent().name:
		$RWallSFX.play()

func update_cash(amount, save=true):
	self.cash += amount
	if save:
		var file = File.new()
		file.open(self.cash_file, File.WRITE)
		file.store_64(clamp(self.cash, 0, pow(2, 63)-1))
		file.close()
	var cash_text = ""
	if self.cash < 1000000:
		cash_text = "%d" % self.cash
	elif self.cash < 1000000000:
		cash_text = "%fM" % self.cash / 1000000
	elif self.cash < 1000000000000:
		cash_text = "%fB" % self.cash / 1000000000
	else:
		cash_text = "%fT" % self.cash / 1000000000000
	$CashLabel.text = "$%s" % cash_text

func _on_Quit_pressed():
	self.quit_game()

func quit_game():
	self.queue_free()
	emit_signal("game_over")

func _on_Purchases_pressed():
	get_tree().paused = true
	$PurchasesPopup.popup()

func _on_NoHatButton_pressed():
	self.set_hat(false)

func _on_PurchasesPopup_popup_hide():
	get_tree().paused = false

func _on_ClosePurchasesButton_pressed():
	$PurchasesPopup.hide()

func scramble_text():
	for node in get_tree().get_nodes_in_group("text"):
		node.text = get_random_text(node.text.length())

func get_random_text(numchars):
	var result = ""
	for i in numchars:
		result += LETTERS[rng.randi_range(0, LETTERS.size() - 1)]
	return result

func _on_WallColorPurchaseButton_pressed():
	self.purchase_color("wall")

func _on_BallColorPurchaseButton_pressed():
	self.purchase_color("ball")

func _on_HatColorPurchaseButton_pressed():
	self.purchase_color("hat")

func _on_PlayerColorPurchaseButton_pressed():
	self.purchase_color("player")

func purchase_color(item_name):
	$PurchaseSFX.play()
	self.other_purchases["color"][item_name]["purchased"] = true
	self.update_cash(self.other_purchases["color"][item_name]["cost"] * -1)
	self.set_purchase_buttons()
	self.save_purchases()

func _on_WallColorPickerButton_color_changed(color):
	self.set_color("wall", color)
	$LWall.modulate = color
	$RWall.modulate = color
	$Roof.modulate = color

func set_color(item, color):
	self.other_purchases["color"][item]["color"] = color
	self.save_colors()

func save_colors():
	var file = File.new()
	file.open(self.colors_file, File.WRITE)
	for item in self.other_purchases["color"]:
		if item == "rows":
			for i in self.colors.size():
				file.store_var(self.colors[i])
		else:
			file.store_var(self.other_purchases["color"][item]["color"])
	file.close()

func read_colors():
	var file = File.new()
	if file.file_exists(self.colors_file):
		file.open(self.colors_file, File.READ)
		for item in self.other_purchases["color"]:
			if item == "rows":
				for i in self.colors.size():
					var color = file.get_var()
					if color:
						self.colors[i] = color
					self.other_purchases["color"][item]["button"].get_child(i).color = self.colors[i]
			else:
				var color = file.get_var()
				if color:
					self.other_purchases["color"][item]["color"] = color
				self.other_purchases["color"][item]["button"].color = self.other_purchases["color"][item]["color"]
	file.close()
	$Ball.modulate = self.other_purchases["color"]["ball"]["color"]
	$LWall.modulate = self.other_purchases["color"]["wall"]["color"]
	$RWall.modulate = self.other_purchases["color"]["wall"]["color"]
	$Roof.modulate = self.other_purchases["color"]["wall"]["color"]

func read_rotations():
	var file = File.new()
	if file.file_exists(self.rotation_file):
		file.open(self.rotation_file, File.READ)
		for item in self.other_purchases["rotation"]:
			var rot = file.get_16()
			self.other_purchases["rotation"][item]["rotation"] = rot
			self.other_purchases["rotation"][item]["button"].set_value(rot)
	file.close()
	self.get_node("Player").rotation = self.other_purchases["rotation"]["player"]["rotation"]
	for node in get_tree().get_nodes_in_group("brick"):
		node.rotation = self.other_purchases["rotation"]["brick"]["rotation"]

func read_speed():
	var file = File.new()
	if file.file_exists(self.speed_file):
		print("reading speed")
		file.open(self.speed_file, File.READ)
		for item in self.other_purchases["speed"]:
			var speed = file.get_16()
			print("read", item, ": ", speed)
			self.other_purchases["speed"][item]["speed"] = speed
			self.other_purchases["speed"][item]["button"].set_value(speed)
		$Ball.direction = Vector2(self.other_purchases["speed"]["ball"]["speed"], 
								self.other_purchases["speed"]["ball"]["speed"])
		self.get_node("Player").speed = self.other_purchases["speed"]["player"]["speed"]
	file.close()

func _on_BallColorPickerButton_color_changed(color):
	self.set_color("ball", color)
	$Ball.modulate = color

func _on_HatColorPickerButton_color_changed(color):
	self.set_color("hat", color)
	for node in get_tree().get_nodes_in_group("hat"):
		node.modulate = color

func _on_PlayerColorPickerButton_color_changed(color):
	self.set_color("player", color)
	self.get_node("Player").get_node("Sprite").modulate = color

func _on_RowColorPicker_color_changed(color):
	self.colors[0] = color
	self.save_colors()

func _on_RowColorPicker2_color_changed(color):
	self.colors[1] = color
	self.save_colors()

func _on_RowColorPicker3_color_changed(color):
	self.colors[2] = color
	self.save_colors()

func _on_RowColorPicker4_color_changed(color):
	self.colors[3] = color
	self.save_colors()

func _on_RowColorPicker5_color_changed(color):
	self.colors[4] = color
	self.save_colors()

func _on_RowColorPicker6_color_changed(color):
	self.colors[5] = color
	self.save_colors()

func _on_RowColorPicker7_color_changed(color):
	self.colors[6] = color
	self.save_colors()

func purchase_rotation(item_name):
	$PurchaseSFX.play()
	self.other_purchases["rotation"][item_name]["purchased"] = true
	self.update_cash(self.other_purchases["rotation"][item_name]["cost"] * -1)
	self.set_purchase_buttons()
	self.save_purchases()

func _on_BrickRotationPurchaseButton_pressed():
	self.purchase_rotation("brick")

func _on_HatRotationPurchaseButton_pressed():
	self.purchase_rotation("hat")

func _on_PlayerRotationPurchaseButton_pressed():
	self.purchase_rotation("player")

func _on_BrickRotationSlider_value_changed(value):
	$PurchasesPopup/BrickRotationValueLabel.text = "%d" % value
	for node in get_tree().get_nodes_in_group("brick"):
		node.rotation = value
	self.set_item_rotation("brick", value)

func _on_PlayerRotationSlider_value_changed(value):
	$PurchasesPopup/PlayerRotationValueLabel.text = "%d" % value
	self.get_node("Player").rotation = value
	self.set_item_rotation("player", value)

func set_item_rotation(item, value):
	self.other_purchases["rotation"][item]["rotation"] = value
	self.save_rotations()

func save_rotations():
	var file = File.new()
	file.open(self.rotation_file, File.WRITE)
	for item in self.other_purchases["rotation"]:
		file.store_16(self.other_purchases["rotation"][item]["rotation"])
	file.close()

func _on_BallSpeedSlider_value_changed(value):
	$PurchasesPopup/BallSpeedValueLabel.text = "%d" % value
	$Ball.direction = Vector2(value, value)
	self.set_speed("ball", value)

func _on_PlayerSpeedSlider_value_changed(value):
	$PurchasesPopup/PlayerSpeedValueLabel.text = "%d" % value
	self.get_node("Player").speed = value
	self.set_speed("player", value)

func set_speed(item, value):
	self.other_purchases["speed"][item]["speed"] = value
	self.save_speed()

func save_speed():
	print("saving speed")
	var file = File.new()
	file.open(self.speed_file, File.WRITE)
	for item in self.other_purchases["speed"]:
		file.store_16(self.other_purchases["speed"][item]["speed"])
	file.close()

func _on_BallSpeedPurchaseButton_pressed():
	self.purchase_speed("ball")

func purchase_speed(item):
	$PurchaseSFX.play()
	self.other_purchases["speed"][item]["purchased"] = true
	self.update_cash(self.other_purchases["speed"][item]["cost"] * -1)
	self.set_purchase_buttons()
	self.save_purchases()

func _on_PlayerSpeedPurchaseButton_pressed():
	self.purchase_speed("player")
